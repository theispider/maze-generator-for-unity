﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : Section
{
    public Vector3Int Position { get; private set; }
    public int SizeX { get; private set; }
    public int SizeZ { get; private set; }


    public Room(Vector3Int position, int sizeX, int sizeZ) : base("Room")
    {
        Position = position;
        SizeX = sizeX;
        SizeZ = sizeZ;
    }

    protected Room(Vector3Int position, int sizeX, int sizeZ, string name) : base(name)
    {
        Position = position;
        SizeX = sizeX;
        SizeZ = sizeZ;
    }
}
