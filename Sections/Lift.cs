﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lift : Room
{
    public new const int SizeX = 1;
    public new const int SizeZ = 1;

    public Lift(Vector3Int position): base(position, SizeX, SizeZ, "Lift")
    {

    }

    public Direction GetDoorDirection()
    {
        return DoorsList[0].Direction;
    }
}
