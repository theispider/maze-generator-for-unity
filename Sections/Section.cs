﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Section
{
    public List<FloorTile> FloorTiles { get; private set; }
    public List<Door> DoorsList { get; private set; }
    //Список соседних секций
    public List<Section> NeighboursList { get; private set; }
    //Список соседних секций, имеющих общие с этой секцией двери
    public List<Section> ConnectedNeighboursList { get; private set; }

    private GameObject gameObject;
    private List<Wall> walls;

    public Section(string sectionName)
    {
        FloorTiles = new List<FloorTile>();
        walls = new List<Wall>();
        NeighboursList = new List<Section>();
        ConnectedNeighboursList = new List<Section>();
        DoorsList = new List<Door>();
        gameObject = new GameObject();
        gameObject.transform.name = sectionName;
    }

    public void AddFloorTile(FloorTile floor)
    {
        FloorTiles.Add(floor);
        floor.transform.SetParent(gameObject.transform);
    }

    public void AddWall(Wall wall)
    {
        walls.Add(wall);
        wall.transform.SetParent(gameObject.transform);
    }

    public void AddDoor(Door door)
    {
        DoorsList.Add(door);
        door.transform.SetParent(gameObject.transform);
    }

    public void AddNeightbour(Section room)
    {
        if (!NeighboursList.Contains(room) && room != this)
        {
            NeighboursList.Add(room);
        }
    }

    public void AddConnectedNeighbours(Section room)
    {
        if (!ConnectedNeighboursList.Contains(room))
        {
            ConnectedNeighboursList.Add(room);
        }
    }
}
