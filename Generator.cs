﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Создает уровень заданного размера.
/// 
/// Создает две комнаты - лифты. Первый лифт - начало уровня, второй лифт - конец уровня.
/// Первый лифт расположен там, где был расположен второй лифт в предыдущем уровне
/// 
/// Создает прямоугольные комнаты в случайных местах, случайного размера
/// 
/// Заполняет свободное место корридорами
/// 
/// Создает двери между комнатами. Проверяет, чтобы к каждой комнате был доступ. Лифт может иметь только одну дверь.
/// 
/// Создает стены
/// </summary>

public class Generator : MonoBehaviour {

    public int LevelSizeX = 10; //размер уровня по оси X
    public int LevelSizeZ = 10; //размер уровня по оси Z
    public int RoomMinSize = 2; //минимальный размер комнаты 
    public int RoomMaxSize = 4; //максимальный размер комнаты
    public int AttemptsCount = 100; //количество попыток генерации комнат
    //public float Delay = 0.1f; //задержка между генерацией объектов

    public FloorTile RoomFloor; //префаб ячейки пола комнаты
    public FloorTile CorridorFloor; //префаб ячейки пола корридора
    public Wall RoomWall; //префаб стены комнаты
    public Wall CorridorWall; //префаб стены корридора
    public Door Door; //префаб двери
    public GameObject LiftPrefab; //префаб лифта, содержащий триггер для перехода на следующий уровень

    private Level currentLevel;

    public void Generate(int level)
    {
        Level previousLevel = currentLevel;
        currentLevel = new Level(LevelSizeX, LevelSizeZ);

        //Генерация лифтов
        GenerateLifts(previousLevel);
        //yield return new WaitForSeconds(Delay);

        //Генерация комнат
        GenerateRooms();
        //yield return new WaitForSeconds(Delay);

        // Генерация корридора
        GenerateCorridors();
        //yield return new WaitForSeconds(Delay);

        //Генерация дверей
        GenerateDoors();
        //yield return new WaitForSeconds(Delay);

        //Генерация стен
        GenerateWalls();
        //yield return new WaitForSeconds(Delay);
    }

    //Генерация лифтов
    void GenerateLifts(Level previousLevel)
    {
        Vector3Int roomPosition = new Vector3Int();

        //Генерация первого лифта
        if (previousLevel == null)
        {
            roomPosition.Set(Random.Range(0, LevelSizeX), 0, (Random.Range(0, LevelSizeZ)));
        }
        else
        {
            roomPosition = previousLevel.EndPosition;
        }
        currentLevel.FirstLift = new Lift(roomPosition);
        GenerateRoom(currentLevel.FirstLift);

        //Генерация второго лифта
        //Уровень обязательно должен содержать два лифта. Если второй лифт случайно пересекся с первым, то выбирается другое расположение
        do
        {
            roomPosition.Set(Random.Range(0, LevelSizeX), 0, (Random.Range(0, LevelSizeZ)));
        }
        while (!currentLevel.IsAreaClear(Lift.SizeX, Lift.SizeZ, roomPosition));
        currentLevel.SecondLift = new Lift(roomPosition);
        GenerateRoom(currentLevel.SecondLift);

        Instantiate(LiftPrefab, currentLevel.SecondLift.Position, Quaternion.identity);
    }

    //Генерация комнат
    private void GenerateRooms()
    {
        Vector3Int roomPosition = new Vector3Int();

        // Генерация комнат
        int roomSizeX;
        int roomSizeZ;
        //Вместо генерации заданного количества комнат, используются попытки сгенерировать комнаты
        //В таком случае сложность всегда линейна, в то время как на генерацию заданного количества комнат каждый раз требуется разное время
        for (int i = 0; i < AttemptsCount; i++)
        {
            roomPosition.Set(Random.Range(0, LevelSizeX), 0, (Random.Range(0, LevelSizeZ)));
            roomSizeX = Random.Range(RoomMinSize, RoomMaxSize + 1);
            roomSizeZ = Random.Range(RoomMinSize, RoomMaxSize + 1);
            if (currentLevel.IsAreaClear(roomSizeX, roomSizeZ, roomPosition))
            {
                GenerateRoom(new Room(roomPosition, roomSizeX, roomSizeZ));
            }
        }
    }

    void GenerateRoom(Room room)
    {
        currentLevel.AddSectionToLevel(room);

        //Заполнение области полом комнаты
        for (int i = 0; i < room.SizeX; i++)
        {
            for (int j = 0; j < room.SizeZ; j++)
            {               
                CreateTile(room.Position + new Vector3Int(i, 0, j), room, RoomFloor);
            }
        }
    }

    //Создает ячейку пола, добавляет ее к комнате и находит соседние ячейки
    void CreateTile(Vector3Int position, Section section, FloorTile tilePrefab)
    {
        FloorTile tile = Instantiate(tilePrefab) as FloorTile;
        tile.Initialize(position, section);
        currentLevel.AddFloorTileToLevel(tile);
        currentLevel.SetNeighbourTiles(tile);
    }


    //Ищет свободные ячейки и заполняет их коридором
    void GenerateCorridors()
    {
        Vector3Int position = new Vector3Int();
        Corridor corridor;

        for (int i = 0; i < LevelSizeX; i++)
        {
            for (int j = 0; j < LevelSizeZ; j++)
            {
                if (currentLevel.FindTile(i, j) == null)
                {
                    position.Set(i, 0, j);
                    corridor = new Corridor();
                    currentLevel.AddSectionToLevel(corridor);
                    FillWithCorridor(position, corridor);
                }
            }
        }
    }

    //Создает соседние ячейки, заполняя пустое пространство рядом
    void FillWithCorridor(Vector3Int position, Corridor corridor)
    {
        CreateTile(position, corridor, CorridorFloor);
        Vector3Int neighbourPosition = new Vector3Int();
        for (int i = 0; i < Directions.Count; i++)
        {
            neighbourPosition.Set(position.x, 0, position.z);
            neighbourPosition += Directions.ToVector3Int((Direction)i);
            if (currentLevel.FindTile(neighbourPosition) == null && currentLevel.IsWithinTheLevel(neighbourPosition))
            {
                FillWithCorridor(neighbourPosition, corridor);
            }
        }
    }
 
    //Связывает каждый коридор с соседними ему комнатами, создавая двери, и проверяет доступность каждой комнаты
    void GenerateDoors()
    {
        Section neighbour;
        List<Section> neighboursList;
        List<Section> connectedNeighboursList;

        foreach (Section section in currentLevel.Sections)
        {
            if(section is Corridor)
            {
                neighboursList = section.NeighboursList;
                connectedNeighboursList = section.ConnectedNeighboursList;
                for (int i = 0; i < section.NeighboursList.Count; i++)
                {
                    neighbour = neighboursList[i];
                    if (!connectedNeighboursList.Contains(neighbour))
                    {
                        //Создает дверь между комнатой и коридором. Создает только одну дверь для лифта
                        if (!(neighbour is Lift) || (neighbour is Lift && neighbour.ConnectedNeighboursList.Count == 0))
                        {
                            ConnectRooms(section, neighbour);
                        }
                    }
                }
            }
        }


        List<Section> checkedSections;
        List<Section> possiblePath;

        //Проверяет можно ли добраться из секции к первому лифту напрямую или через другие секции
        foreach (Section section in currentLevel.Sections)
        {
            checkedSections = new List<Section>();
            if (section != currentLevel.FirstLift)
            {
                //Если доступа нет, то пытается создать новые двери, если доступа нету
                if (!HasAccessToFirstLift(section, checkedSections))
                {
                    possiblePath = new List<Section>();
                    //Если через комнаты можно провести путь и комнаты еще не связаны, то между ними создаются двери
                    if (FindPathToFirstLift(section, checkedSections, possiblePath))
                    {
                        for(int i = 0; i < possiblePath.Count - 1; i ++)
                        {
                            if (!possiblePath[i].ConnectedNeighboursList.Contains(possiblePath[i + 1]))
                            {
                                ConnectRooms(possiblePath[i], possiblePath[i + 1]);
                            }
                        }
                    }
                    else
                    {
                        print("Can't find path to lift");
                    }
                }
            }
        }
    }

    //Ищет соседние ячейки пола комнат, выбирает случайную пару и ставит там двери
    void ConnectRooms(Section firstCorridor, Section secondCorridor)
    {
        //Выбирает клетки
        List<FloorTile> possibleNeighbourTiles = new List<FloorTile>();
        List<Direction> possibleDirections = new List<Direction>();
        for (int i = 0; i < secondCorridor.FloorTiles.Count; i++)
        {
            for (int j = 0; j < Directions.Count; j++)
            {
                Direction dir = (Direction)j;
                if (secondCorridor.FloorTiles[i].GetNeightbour(dir) != null 
                    && secondCorridor.FloorTiles[i].GetNeightbour(dir).Section == firstCorridor)
                {
                    possibleNeighbourTiles.Add(secondCorridor.FloorTiles[i]);
                    possibleDirections.Add((Direction)j);
                }
            }
        }

        //Выбирает случайную пару
        int randomTileIndex = Random.Range(0, possibleNeighbourTiles.Count);
        FloorTile randomTile = possibleNeighbourTiles[randomTileIndex];
        Direction direction = possibleDirections[randomTileIndex];

        //Связывает комнаты и создает двери
        Vector3Int neighbourTilePosition = randomTile.Position + Directions.ToVector3Int(direction);
        firstCorridor.AddConnectedNeighbours(secondCorridor);
        secondCorridor.AddConnectedNeighbours(firstCorridor);
        firstCorridor.AddDoor(SetDoor(neighbourTilePosition, Directions.GetOpposite(direction)));
        secondCorridor.AddDoor(SetDoor(randomTile.Position, direction));
    }

    //Создает и помещает дверь в нужное место
    Door SetDoor(Vector3Int position, Direction direction)
    {
        //Door door = Instantiate(Door, position, Directions.ToRotation(direction));
        Door door = SetWall(position, direction, Door) as Door;
        currentLevel.FindTile(position).SetWall(door, direction);
        door.Direction = direction;
        return door;
    }

    //Проверяет, есть ли у комнаты доступ к первому лифту, перемещаясь от комнаты к комнате
    bool HasAccessToFirstLift(Section section, List<Section> checkedCorridors)
    {
        checkedCorridors.Add(section);
        List<Section> accessibleSections = section.ConnectedNeighboursList;
        for (int i = 0; i < accessibleSections.Count; i++)
        {
            if (accessibleSections[i] == currentLevel.FirstLift)
            {
                return true;
            }
        }
        for (int i = 0; i < accessibleSections.Count; i++)
        {
            if (!checkedCorridors.Contains(accessibleSections[i]) && HasAccessToFirstLift(accessibleSections[i], checkedCorridors))
            {
                return true;
            }
        }
        return false;
    }

    //Пытается найти путь через комнаты, чтобы получить доступ к первому лифту
    bool FindPathToFirstLift(Section corridor, List<Section> checkedCorridors, List<Section> possiblePath)
    {
        checkedCorridors.Add(corridor);
        possiblePath.Add(corridor);
        List<Section> neighbourCorridors = corridor.NeighboursList;
        List<Section> acceccibleCorridors = corridor.ConnectedNeighboursList;

        //Проверяет, есть ли у соседей доступ к лифту
        if (acceccibleCorridors.Count > 0)
        {
            for (int i = 0; i < acceccibleCorridors.Count; i++)
            {
                if (acceccibleCorridors[i] == currentLevel.FirstLift)
                {
                    return true;
                }
                else if (!(acceccibleCorridors[i] is Lift))
                {
                    List<Section> checkedSections = new List<Section>();
                    HasAccessToFirstLift(acceccibleCorridors[i], checkedSections);
                }
            }
        }

        //Продолжает путь
        for (int i = 0; i < neighbourCorridors.Count; i++)
        {
            if (!checkedCorridors.Contains(neighbourCorridors[i]) && !(neighbourCorridors[i] is Lift) 
                && FindPathToFirstLift(neighbourCorridors[i], checkedCorridors, possiblePath))
            {
                return true;
            }
        }
        possiblePath.Remove(corridor);
        return false;
    }

    //Ставит стены между коридорами и комнатами, а также на границах уровня
    void GenerateWalls()
    {
        FloorTile tile;
        Wall wallPrefab;

        for (int i = 0; i < LevelSizeX; i++)
        {
            for (int j = 0; j < LevelSizeZ; j++)
            {
                tile = currentLevel.FindTile(i, j);
                if (tile.Section is Room)
                    wallPrefab = RoomWall;
                else
                    wallPrefab = CorridorWall;

                if (i == 0)
                {
                    tile.Section.AddWall(SetWall(tile.Position, Direction.West, wallPrefab));
                }
                else if (i == LevelSizeX - 1)
                {
                    tile.Section.AddWall(SetWall(tile.Position, Direction.East, wallPrefab));
                }

                if (j == 0)
                {
                    tile.Section.AddWall(SetWall(tile.Position, Direction.South, wallPrefab));
                }
                else if (j == LevelSizeZ - 1)
                {
                    tile.Section.AddWall(SetWall(tile.Position, Direction.North, wallPrefab));
                }

                for (int dir = 0; dir < Directions.Count; dir++)
                {
                    Direction direction = (Direction)dir;
                    if (tile.GetWall(direction) == null && tile.GetNeightbour(direction) != null 
                        && tile.GetNeightbour(direction).Section != tile.Section)
                    {
                        tile.Section.AddWall(SetWall(tile.Position, (Direction)dir, wallPrefab));
                    }
                }
            }
        }
    }

    //Создает и помещает объект стены
    Wall SetWall(Vector3Int position, Direction direction, Wall wallPrefab)
    {
        Wall wall = Instantiate(wallPrefab, position, Directions.ToRotation(direction));
        currentLevel.FindTile(position).SetWall(wall, direction);
        return wall;
    }

}
