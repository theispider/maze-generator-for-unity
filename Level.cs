﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Содержит информацию о новом уровне
class Level
{
    public static Level Instance;

    //Расположение первого лифта, начало уровня
    public Vector3Int StartPosition { get { return FirstLift.Position; } } 
    //Направление двери лифта
    public Quaternion StartRotation { get { return Directions.ToRotation(FirstLift.GetDoorDirection()); } } 
    //Расположение второго лифта, конец уровня
    public Vector3Int EndPosition { get { return SecondLift.Position; } }
    public Lift FirstLift { get; set; }
    public Lift SecondLift { get; set; }

    //Список всех секций - комнат, коридоров и лифтов
    public List<Section> Sections { get; private set; }

    private int levelSizeX;
    private int levelSizeZ;

    //Список ячеек пола
    private FloorTile[,] floorTiles;

    public Level(int LevelSizeX, int LevelSizeZ)
    {
        Instance = this;
        levelSizeX = LevelSizeX;
        levelSizeZ = LevelSizeZ;

        floorTiles = new FloorTile[levelSizeX, levelSizeZ];
        Sections = new List<Section>();
    }

    //Добавляет секцию к уровню
    public void AddSectionToLevel(Section section)
    {
        if (!Sections.Contains(section))
            Sections.Add(section);
    }

    //Добавляет ячейку пола к уровню
    public void AddFloorTileToLevel(FloorTile tile)
    {
        floorTiles[tile.Position.x, tile.Position.z] = tile;
    }

    //Проверяет, свободна ли область. Комнаты прямоугольные. Расположение комнаты - расположение ее левого нижнего угла
    public bool IsAreaClear(int roomSizeX, int roomSizeZ, Vector3Int roomPosition)
    {
        Vector3Int position;
        for (int i = 0; i < roomSizeX; i++)
        {
            for (int j = 0; j < roomSizeZ; j++)
            {
                position = new Vector3Int(i + roomPosition.x, 0, j + roomPosition.z);
                //Область занята, если она находится вне уровня, или есть пересечение с другой комнатой
                if (!IsWithinTheLevel(position) || floorTiles[position.x, position.z] != null)
                {
                    return false;
                }
            }
        }
        return true;
    }

    //Ищет соседей ячейки и сохраняет их
    public void SetNeighbourTiles(FloorTile tile)
    {
        FloorTile neightbour;
        Vector3Int neighbourPosition;

        //Проверяет каждую сторону
        for (int i = 0; i < Directions.Count; i++)
        {
            neighbourPosition = tile.Position + Directions.ToVector3Int((Direction)i);
            neightbour = FindTile(neighbourPosition);

            if (neightbour != null)
            {
                tile.SetNeightbour(neightbour, (Direction)i);
                neightbour.SetNeightbour(tile, Directions.GetOpposite((Direction)i));
            }
        }
    }

    //Возвращает ячейку пола, если она находится в пределах уровня
    public FloorTile FindTile(Vector3Int position)
    {
        if (IsWithinTheLevel(position))
        {
            return floorTiles[position.x, position.z];
        }
        return null;
    }

    //Возвращает ячейку пола, если она находится в пределах уровня
    public FloorTile FindTile(int positionX, int positionZ)
    {
        return FindTile(new Vector3Int(positionX, 0, positionZ));
    }


    //Проверяет, находится ли точка в пределах уровня
    public bool IsWithinTheLevel(Vector3Int position)
    {
        if (position.x >= 0 && position.x < levelSizeX && position.z >= 0 && position.z < levelSizeZ)
        {
            return true;
        }
        return false;
    }
}

