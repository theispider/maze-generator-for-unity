﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorTile : MonoBehaviour {

    public Vector3Int Position { get; private set; }
    public Section Section { get; private set; }

    private FloorTile[] neighbourTiles;
    private Wall[] walls;



    public void Initialize(Vector3Int position, Section section)
    {
        neighbourTiles = new FloorTile[Directions.Count];
        walls = new Wall[Directions.Count];

        Position = position;
        transform.position = position;
        Section = section;
        Section.AddFloorTile(this);
    }

    public void SetNeightbour(FloorTile tile, Direction dir)
    {
        neighbourTiles[(int)dir] = tile;
        Section.AddNeightbour(tile.Section);
    }

    public FloorTile GetNeightbour(Direction dir)
    {
        return neighbourTiles[(int)dir];
    }

    public void SetWall(Wall wall, Direction direction)
    {
        walls[(int)direction] = wall;
    }

    public Wall GetWall(Direction direction)
    {
        return walls[(int)direction];
    }
}
