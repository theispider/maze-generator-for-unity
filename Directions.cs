﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    North,
    East,
    South,
    West
}

public static class Directions
{
    public static int Count { get { return 4; } }

    public static Quaternion ToRotation(Direction dir)
    {
        return rotations[(int)dir];
    }

    public static Vector3Int ToVector3Int(Direction dir)
    {
        return vectors[(int)dir];
    }

    public static Direction GetOpposite(Direction direction)
    {
        return opposites[(int)direction];
    }

    public static Direction GetRandomDirection()
    {
        return (Direction)Random.Range(0, Count);
    }

    private static Quaternion[] rotations =
    {
        Quaternion.identity,
        Quaternion.Euler(0f, 90f, 0f),
        Quaternion.Euler(0f, 180f, 0f),
        Quaternion.Euler(0f, 270f, 0f)
    };

    private static Vector3Int[] vectors =
    {
        new Vector3Int(0, 0, 1),
        new Vector3Int(1, 0, 0),
        new Vector3Int(0, 0, -1),
        new Vector3Int(-1, 0, 0),
    };

    private static Direction[] opposites =
    {
        Direction.South,
        Direction.West,
        Direction.North,
        Direction.East
        };
}
