﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : Wall, IInteractableObject
{
    public Direction Direction;

    //Взаимодействие с дверью
    public void Interact()
    {
        GameObject child = gameObject.transform.GetChild(0).gameObject;
        child.SetActive(!child.activeInHierarchy);
    }
}
